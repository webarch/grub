# Webarchitects Grub Ansible Role

An Ansible role to install and configure `grub2`.

## Defaults

See the [defaults/main.yml](defaults/main.yml) file for the default variables, the [vars/main.yml](vars/main.yml) file for the preset variables and the [meta/argument_specs.yml](meta/argument_specs.yml) file for the variable specification.

### grub

The `grub` variable is required to be defined, by default it is `false`, when it is `true` all tasks in this role will be run.

### grub_defaults

A dictionary of entries that should be present in the `/etc/defaults/grub` file, for example:

```yaml
grub_defaults:
  GRUB_CMDLINE_LINUX_DEFAULT: "vsyscall=emulate"
```

### grub_verify

Verify all variables using the [meta/argument_specs.yml](meta/argument_specs.yml), `grub_verify` defaults to `true`.

## Copyright

Copyright 2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
